<?php
/**
 * Created by PhpStorm.
 * User: xing.chen
 * Date: 2019/7/26
 * Time: 18:17
 */

return [
    'controllerNamespace' => 'xing\ace\modules\admin\controllers',
    'defaultRoute'=>'admin/index/index',
    // 为modules追加这个配置（不要全替换）
    'modules' => [
        'admin' => [
            'class' => 'xing\ace\modules\admin\Module',
        ],
    ],
    'components' => [
        'user' => [
            'identityClass' => 'xing\ace\modules\admin\models\AdminUser',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '__ace_identity', 'httpOnly' => true],
            'idParam' => '__ace_admin',
            'loginUrl' => ['admin/site/login'],
        ],
    ],
];