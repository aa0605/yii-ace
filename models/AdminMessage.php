<?php

namespace xing\ace\models;

use Yii;

/**
 * This is the model class for table "admin_message".
 *
 * @property int $msgId
 * @property string|null $roleId 管理组
 * @property int|null $adminId 管理员
 * @property int|null $type 消息类型
 * @property int|null $level 紧急级别
 * @property string|null $code 程序代码
 * @property string|null $title 标题
 * @property int|null $isRead 是否已读
 * @property int|null $handleStatus 处理状态:-1不需要处理 0待处理 2已指派 4处理中 9已完成 -2以后处理
 * @property string|null $content 内容
 * @property string|null $createTime 创建时间
 */
class AdminMessage extends \xing\ace\models\BaseActiveModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'admin_message';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['roleId', 'createTime', 'code'], 'safe'],
            [['adminId', 'type', 'level', 'isRead', 'handleStatus'], 'integer'],
            [['title'], 'string', 'max' => 200],
            [['content'], 'string', 'max' => 10000],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'msgId' => 'Msg ID',
            'roleId' => '管理组',
            'adminId' => '管理员',
            'type' => '消息类型',
            'level' => '紧急级别',
            'code' => '程序代码',
            'title' => '标题',
            'isRead' => '是否已读',
            'handleStatus' => '处理状态',
            'content' => '内容',
            'createTime' => '创建时间',
        ];
    }

    public static function create($roleId, $adminId, $type, $code = null, $level = null, $handleStatus = null, $title = null, $contetn = null)
    {
        $m = new static;
        $m->roleId = $roleId;
        $m->adminId = $adminId;
        $m->type = $type;
        $m->code = $code;
        $m->level = $level;
        $m->handleStatus = $handleStatus;
        $m->title = $title;
        $m->content = $contetn;
        $m->createTime = date('Y-m-d H:i:s');
        return $m->logicSave();
    }
}
