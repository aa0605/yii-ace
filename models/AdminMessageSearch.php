<?php

namespace xing\ace\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use xing\ace\models\AdminMessage;

/**
 * AdminMessageSearch represents the model behind the search form of `xing\ace\models\AdminMessage`.
 */
class AdminMessageSearch extends AdminMessage
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['msgId', 'roleId', 'adminId', 'type', 'level', 'isRead', 'handleStatus'], 'integer'],
            [['code', 'title', 'content', 'createTime'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = static::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    $this->primaryKey()[0] => SORT_DESC,
                ]
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'msgId' => $this->msgId,
            'roleId' => $this->roleId,
            'adminId' => $this->adminId,
            'type' => $this->type,
            'level' => $this->level,
            'isRead' => $this->isRead,
            'handleStatus' => $this->handleStatus,
            'createTime' => $this->createTime,
        ]);

        $query->andFilterWhere(['like', 'code', $this->code])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'content', $this->content]);

        return $dataProvider;
    }
}
