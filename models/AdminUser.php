<?php
// +----------------------------------------------------------------------
// | When work is a pleasure, life is a joy!
// +----------------------------------------------------------------------
// | User: ShouKun Liu  |  Email:24147287@qq.com  | Time:2016/12/10 15:36
// +----------------------------------------------------------------------
// | TITLE: 用户
// +----------------------------------------------------------------------


namespace xing\ace\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "admin_user".
 *
 * @property int $id 主键
 * @property int|null $companyId 公司
 * @property string|null $username 用户名
 * @property string|null $auth_key 盐
 * @property string|null $email 邮件
 * @property int|null $status 状态,1 启用 0禁用
 * @property string|null $mobile 手机
 * @property string|null $timezone 时区，默认8区，东八区，北京/上海
 * @property string|null $code 业务员推荐码
 * @property int|null $role_id 管理权限组
 * @property int|null $created_at 创建时间
 * @property int|null $updated_at 提示
 * @property string|null $access_token token
 * @property string|null $password_hash 密码
 * @property string|null $password_reset_token 重置密码密钥
 *
 * @property Company $company
 * @property AdminRole $role
 * @property CompanyPersonnel[] $companyPersonnels
 */
class AdminUser extends BaseActiveModel implements IdentityInterface
{

    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';

    /**
     * 用户修改
     */
    const SCENARIO_USER_UPDATE = 'user_update';

    /**
     * 用户状态
     */
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 1;

    public $password;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['username', 'required', 'message' => '名称必须'],
            [['companyId', 'status', 'created_at', 'updated_at'], 'integer'],
            [['username', 'email', 'access_token', 'password_hash', 'password_reset_token'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['mobile'], 'string', 'max' => 15],
            [['timezone'], 'string', 'max' => 300],
            [['code'], 'string', 'max' => 30],
            ['created_at', 'default', 'value' => self::getDate()],
            ['updated_at', 'default', 'value' => self::getDate()],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['mobile', 'match','pattern'=>'/^1[0-9]{10}$/','message'=>'{attribute}不是正确的手机号'],
        ];
    }

    public static function tableName()
    {
        return '{{%admin_user}}';
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();

        $scenarios[self::SCENARIO_CREATE] =
            [
                'username',
                'auth_key',
                'password_hash',
                'password_reset_token',
                'email',
                'code',
                'created_at',
                'status',
                'role_id',
                'mobile'
            ];

        $scenarios[self::SCENARIO_UPDATE] =
            [
                'username',
                'auth_key',
                'password_hash',
                'password_reset_token',
                'email',
                'created_at',
                'status',
                'role_id',
                'mobile'
            ];
        $scenarios[self::SCENARIO_USER_UPDATE] = [
            'password_hash',
            'email',
            'mobile'
        ];
        return $scenarios;

    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => '主键',
            'companyId' => '公司',
            'username' => '用户名',
            'auth_key' => '盐',
            'email' => '邮件',
            'status' => '状态,1 启用 0禁用',
            'mobile' => '手机',
            'timezone' => '时区，默认8区，东八区，北京/上海',
            'code' => '业务员推荐码',
            'role_id' => '管理权限组',
            'created_at' => '创建时间',
            'updated_at' => '提示',
            'access_token' => 'token',
            'password_hash' => '密码',
            'password_reset_token' => '重置密码密钥',
        ];
    }

    public function attributeValues()
    {
        return [
            'status' => [
                '0' => '停用',
                '1' => '正常',
            ]
        ];

    }

    public static function getDate()
    {
        return date('Y-m-d H:i:s');
    }

    public static function deleteUser($id)
    {
        $model = self::findOne($id);
        if ($model) {
            $model->scenarios(self::SCENARIO_UPDATE);
            $model->status = self::STATUS_DELETED;
            $model->save();
            return ($model->save()) ? true : false;
        } else {
            return false;
        }
    }

    /**
     * 获取用户角色
     * @return array|null|\yii\db\ActiveRecord
     */
    public function getAdminRole()
    {
        return $this->hasOne(AdminRole::className(), ['id' => 'role_id'])->asArray()->one();
    }


    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            [
                'class' => \xing\helper\yii\behavior\Array2stringBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['role_id'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['role_id'],
                ]
            ]
        ];
    }


    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }

    public function generateAccessToken()
    {
        return $this->access_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }
}
