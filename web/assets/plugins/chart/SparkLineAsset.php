<?php
namespace xing\ace\web\assets\plugins\chart;

use xing\ace\web\assets\AceBundleAsset;

class SparkLineAsset extends AceBundleAsset
{
    public $js = [
       'js/jquery.sparkline.index.min.js'
    ];

    public $depends = [
        'xing\ace\web\assets\plugins\JqueryUIAsset'
    ];
}