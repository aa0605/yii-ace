<?php
namespace xing\ace\web\assets\plugins\chart;

use xing\ace\web\assets\AceBundleAsset;

class EasyPieChartAsset extends AceBundleAsset
{
    public $js = [
       'js/jquery.easypiechart.min.js'
    ];

    public $depends = [
        'xing\ace\web\assets\plugins\JqueryUIAsset'
    ];
}