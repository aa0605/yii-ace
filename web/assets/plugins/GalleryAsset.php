<?php
namespace xing\ace\web\assets\plugins;

use xing\ace\web\assets\AceBundleAsset;

class GalleryAsset extends AceBundleAsset
{
    public $css = [
        'css/colorbox.min.css'
    ];

    public $js = [
        'js/jquery.colorbox.min.js',
    ];

    public $depends = [
        'yii\bootstrap\BootstrapPluginAsset',
        'xing\ace\web\assets\FontAwesomeAsset',
    ];
}