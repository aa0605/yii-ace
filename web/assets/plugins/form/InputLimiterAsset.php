<?php
namespace xing\ace\web\assets\plugins\form;

use xing\ace\web\assets\AceBundleAsset;

class InputLimiterAsset extends AceBundleAsset
{
    public $js = [
        'js/jquery.inputlimiter.min.js'
    ];

    public $depends = [
        'xing\ace\web\assets\plugins\JqueryUIAsset'
    ];
}