<?php
namespace xing\ace\web\assets\plugins\form;

use yii\web\AssetBundle;

class WDatePicketAsset extends AssetBundle
{
    public $sourcePath = '@xing/ace/assets/My97DatePicker';

    public $js = [
        'WdatePicker.js'
    ];

    public $depends = [];
}