<?php
namespace xing\ace\web\assets\plugins\form;

use xing\ace\web\assets\AceBundleAsset;

class DateRangePickerAsset extends AceBundleAsset
{
    public $css = [
        'css/daterangepicker.min.css'
    ];

    public $js = [
        'js/moment.min.js',
        'js/daterangepicker.min.js',
    ];

    public $depends = [
        'xing\ace\web\assets\plugins\JqueryUIAsset'
    ];
}