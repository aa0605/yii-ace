<?php
namespace xing\ace\web\assets\plugins\form;

use xing\ace\web\assets\AceBundleAsset;

class MarkdownAsset extends AceBundleAsset
{
    public $js = [
        'js/markdown.min.js',
        'js/bootstrap-markdown.min.js'
    ];

    public $depends = [
        'xing\ace\web\assets\plugins\JqueryUIAsset'
    ];
}