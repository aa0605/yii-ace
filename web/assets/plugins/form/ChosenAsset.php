<?php
namespace xing\ace\web\assets\plugins\form;

use xing\ace\web\assets\AceBundleAsset;

class ChosenAsset extends AceBundleAsset
{
    public $css = [
        'css/chosen.min.css'
    ];

    public $js = [
        'js/chosen.jquery.min.js'
    ];

    public $depends = [
        'xing\ace\web\assets\plugins\JqueryUIAsset'
    ];
}