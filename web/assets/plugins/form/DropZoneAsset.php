<?php
namespace xing\ace\web\assets\plugins\form;

use xing\ace\web\assets\AceBundleAsset;

class DropZoneAsset extends AceBundleAsset
{
    public $css = [
        'css/dropzone.min.css'
    ];

    public $js = [
        'js/dropzone.min.js'
    ];

    public $depends = [
        'yii\bootstrap\BootstrapPluginAsset',
        'xing\ace\web\assets\FontAwesomeAsset',
    ];
}