<?php
namespace xing\ace\web\assets\plugins\form;

use xing\ace\web\assets\AceBundleAsset;

class ColorPickerAsset extends AceBundleAsset
{
    public $css = [
        'css/bootstrap-colorpicker.min.css'
    ];

    public $js = [
        'js/bootstrap-colorpicker.min.js',
    ];

    public $depends = [
        'xing\ace\web\assets\plugins\JqueryUIAsset'
    ];
}