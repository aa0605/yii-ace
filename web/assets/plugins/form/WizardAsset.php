<?php
namespace xing\ace\web\assets\plugins\form;

use xing\ace\web\assets\AceBundleAsset;

class WizardAsset extends AceBundleAsset
{
    public $css = [
        'css/select2.min.css'
    ];

    public $js = [
        'js/wizard.min.js',
        'js/select2.min.js'
    ];

    public $depends = [
        'xing\ace\web\assets\plugins\JqueryUIAsset',
        'xing\ace\web\assets\plugins\tools\ValidateAsset',
        'xing\ace\web\assets\plugins\tools\AdditionalMethodsAsset',
        'xing\ace\web\assets\plugins\tools\BootboxAsset',
        'xing\ace\web\assets\plugins\form\MaskedInputAsset',
    ];
}