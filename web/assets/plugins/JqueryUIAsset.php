<?php
namespace xing\ace\web\assets\plugins;

use xing\ace\web\assets\AceBundleAsset;

class JqueryUIAsset extends AceBundleAsset
{
    public $css = [
        'css/jquery-ui.custom.min.css',
    ];

    public $js = [
        ['js/excanvas.min.js', 'condition'=>'lte IE 8'],
        'js/jquery-ui.custom.min.js',
        'js/jquery.ui.touch-punch.min.js',
    ];

    public $depends = [
        'yii\bootstrap\BootstrapPluginAsset',
        'xing\ace\web\assets\FontAwesomeAsset',
    ];
}