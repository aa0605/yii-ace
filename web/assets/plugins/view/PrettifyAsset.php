<?php
namespace xing\ace\web\assets\plugins\view;

use xing\ace\web\assets\AceBundleAsset;

class PrettifyAsset extends AceBundleAsset
{
    public $css = [
        'css/prettify.min.css'
    ];

    public $js = [
        'js/prettify.min.js',
    ];

    public $depends = [
        'yii\bootstrap\BootstrapPluginAsset',
        'xing\ace\web\assets\FontAwesomeAsset',
    ];
}