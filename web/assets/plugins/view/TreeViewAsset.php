<?php
namespace xing\ace\web\assets\plugins\view;

use xing\ace\web\assets\AceBundleAsset;

class TreeViewAsset extends AceBundleAsset
{
    public $js = [
        'js/tree.min.js',
    ];

    public $depends = [
        'yii\bootstrap\BootstrapPluginAsset',
        'xing\ace\web\assets\FontAwesomeAsset',
    ];
}