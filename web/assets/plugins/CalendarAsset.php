<?php
namespace xing\ace\web\assets\plugins;

use xing\ace\web\assets\AceBundleAsset;

class CalendarAsset extends AceBundleAsset
{
    public $css = [
        'css/fullcalendar.min.css'
    ];

    public $js = [
        'js/moment.min.js',
        'js/fullcalendar.min.js',
    ];

    public $depends = [
        'xing\ace\web\assets\plugins\JqueryUIAsset',
        'xing\ace\web\assets\plugins\tools\BootboxAsset',
    ];
}