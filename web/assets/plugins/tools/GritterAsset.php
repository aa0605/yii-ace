<?php
namespace xing\ace\web\assets\plugins\tools;

use xing\ace\web\assets\AceBundleAsset;

/**
 * Class GritterAsset
 * @package xing\ace\web\assets\plugins\tools
 */
class GritterAsset extends AceBundleAsset
{
    public $js = [
        'js/jquery.gritter.min.js'
    ];

    public $depends = [
        'xing\ace\web\assets\plugins\JqueryUIAsset'
    ];
}