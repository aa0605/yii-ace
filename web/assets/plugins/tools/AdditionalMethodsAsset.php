<?php
namespace xing\ace\web\assets\plugins\tools;

use xing\ace\web\assets\AceBundleAsset;

class AdditionalMethodsAsset extends AceBundleAsset
{
    public $js = [
        'jquery-additional-methods.min.js'
    ];

    public $depends = [
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}