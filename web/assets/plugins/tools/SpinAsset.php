<?php
namespace xing\ace\web\assets\plugins\tools;

use xing\ace\web\assets\AceBundleAsset;

/**
 * Class SpinAsset
 * @package xing\ace\web\assets\plugins\tools
 */
class SpinAsset extends AceBundleAsset
{
    public $js = [
        'js/spin.js'
    ];

    public $depends = [
        'xing\ace\web\assets\plugins\JqueryUIAsset'
    ];
}