<?php
namespace xing\ace\web\assets\plugins\tools;

use xing\ace\web\assets\AceBundleAsset;

class BootboxAsset extends AceBundleAsset
{
    public $js = [
        'js/bootbox.js'
    ];

    public $depends = [
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}