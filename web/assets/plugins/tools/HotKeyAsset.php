<?php
namespace xing\ace\web\assets\plugins\tools;

use xing\ace\web\assets\AceBundleAsset;

class HotKeyAsset extends AceBundleAsset
{
    public $js = [
        'js/jquery.hotkeys.index.min.js'
    ];

    public $depends = [
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}