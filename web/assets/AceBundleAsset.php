<?php
namespace xing\ace\web\assets;

use yii\web\AssetBundle;

class AceBundleAsset extends AssetBundle
{
    public $sourcePath = '@xing/ace/assets/ace/assets';
}