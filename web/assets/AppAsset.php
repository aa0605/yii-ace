<?php

namespace xing\ace\web\assets;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * Configuration for Ace Admin client script files
 */
class AppAsset extends AssetBundle
{
    public $sourcePath = '@xing/ace/assets/web';

    public $css = [
        'css/style.css'
    ];

    public $js = [
        'js/tools.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'xing\ace\web\assets\AceAsset',
        //'xing\ace\web\assets\plugins\JqueryUIAsset',
        'xing\ace\web\assets\AceScriptAsset',
    ];
}