<?php
/**
 * Created by perpel.
 * Created on 2018/1/2 0002 16:00
 */

namespace xing\ace\web\assets;

class UploadAsset extends \yii\web\AssetBundle
{
    public $sourcePath = '@xing/ace/assets/web';

    public $js = [
        'js/xing.upload.js'
    ];

    public $depends = [
        'yii\web\JqueryAsset',
        'xing\ace\web\assets\plugins\tools\BootboxAsset',
    ];
}