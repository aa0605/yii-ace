<?php

namespace xing\ace;

use xing\ace\traits\JsonTrait;
use Yii;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\UnauthorizedHttpException;

/**
 * ace module definition class
 */
class ModuleApi extends \yii\base\Module
{
    use JsonTrait;
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'xing\ace\controllersApi';

    /**
     * @var string 定义使用布局
     */
    public $layout = false;

    /**
     * @var bool 权限验证
     */
    public $verifyAuthority = true;



    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
    }


    /**
     * 获取登录用户
     *
     * @return null|object
     * @throws \yii\base\InvalidConfigException
     */
    public function getUser()
    {
        return Yii::$app->user->identity;
    }

}
