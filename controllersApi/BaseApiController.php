<?php


namespace xing\ace\controllersApi;


use common\map\api\ApiMap;
use common\map\api\ResponseMap;
use xing\ace\map\ResponseApiMap;
use xing\ace\traits\RbacTrait;
use xing\ace\traits\RenderTrait;
use xing\helper\resource\ReturnHelperBase;
use xing\helper\yii\ReturnHelper;
use Yii;
use yii\db\ActiveRecord;
use yii\filters\AccessControl;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\web\Response;

class BaseApiController extends \xingchen\adminApi\yii\YiiBaseApiController
{

    use RbacTrait;
    public $layout = false;

    public function getLgoinId()
    {
        return Yii::$app->user->identity->getId();
    }
}
