<?php

namespace xing\ace\controllersApi;

use xing\ace\models\AdminMessage;
use Yii;

/**
 * AdminMessageController implements the CRUD actions for AdminMessage model.
 */
class AdminMessageController extends BaseApiController
{
    public $modelSearchClass = 'xing\ace\models\AdminMessageSearch';
    public $modelClass = 'xing\ace\models\AdminMessage';

    public function actionNew()
    {
        $user = Yii::$app->user->identity ?? null;
        $count = 0;
        if (!empty($user)) {
            $model = AdminMessage::find()->orWhere(['roleId' => $user->role_id, 'adminId' => null])
                ->orWhere(['adminId' => $user->id])
                ->andWhere(['isRead' => 0]);
            $count = $model->count();
            $list = $model->limit(3)->all();
        }
        return ['unread' => $count, 'list' => $list];
    }
}
