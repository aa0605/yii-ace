<?php
/**
 * Created by perpel.
 * Created on 2018/1/2 0002 16:00
 */

namespace xing\ace\modules\admin\assets;

class EditorMdAsset extends \xing\editormd\EditorMdAsset
{
    public $depends = [
        'xing\ace\modules\admin\assets\AceAsset',
    ];
}