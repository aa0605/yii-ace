<?php
namespace xing\ace\modules\admin\assets;

use yii\web\AssetBundle;

class AceBundleAsset extends AssetBundle
{
    public $sourcePath = '@xing/ace/assets/ace/assets';
}