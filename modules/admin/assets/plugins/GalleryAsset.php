<?php
namespace xing\ace\modules\admin\assets\plugins;

use xing\ace\modules\admin\assets\AceBundleAsset;

class GalleryAsset extends AceBundleAsset
{
    public $css = [
        'css/colorbox.min.css'
    ];

    public $js = [
        'js/jquery.colorbox.min.js',
    ];

    public $depends = [
        'yii\bootstrap\BootstrapPluginAsset',
        'xing\ace\modules\admin\assets\FontAwesomeAsset',
    ];
}