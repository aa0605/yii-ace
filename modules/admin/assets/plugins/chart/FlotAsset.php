<?php
namespace xing\ace\modules\admin\assets\plugins\chart;

use xing\ace\modules\admin\assets\AceBundleAsset;

class FlotAsset extends AceBundleAsset
{
    public $js = [
        'js/jquery.flot.min.js',
        'js/jquery.flot.pie.min.js',
        'js/jquery.flot.resize.min.js'
    ];

    public $depends = [
        'xing\ace\modules\admin\assets\plugins\JqueryUIAsset'
    ];
}