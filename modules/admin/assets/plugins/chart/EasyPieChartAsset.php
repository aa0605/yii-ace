<?php
namespace xing\ace\modules\admin\assets\plugins\chart;

use xing\ace\modules\admin\assets\AceBundleAsset;

class EasyPieChartAsset extends AceBundleAsset
{
    public $js = [
       'js/jquery.easypiechart.min.js'
    ];

    public $depends = [
        'xing\ace\modules\admin\assets\plugins\JqueryUIAsset'
    ];
}