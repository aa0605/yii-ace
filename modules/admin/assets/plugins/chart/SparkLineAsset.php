<?php
namespace xing\ace\modules\admin\assets\plugins\chart;

use xing\ace\modules\admin\assets\AceBundleAsset;

class SparkLineAsset extends AceBundleAsset
{
    public $js = [
       'js/jquery.sparkline.index.min.js'
    ];

    public $depends = [
        'xing\ace\modules\admin\assets\plugins\JqueryUIAsset'
    ];
}