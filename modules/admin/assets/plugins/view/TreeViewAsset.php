<?php
namespace xing\ace\modules\admin\assets\plugins\view;

use xing\ace\modules\admin\assets\AceBundleAsset;

class TreeViewAsset extends AceBundleAsset
{
    public $js = [
        'js/tree.min.js',
    ];

    public $depends = [
        'yii\bootstrap\BootstrapPluginAsset',
        'xing\ace\modules\admin\assets\FontAwesomeAsset',
    ];
}