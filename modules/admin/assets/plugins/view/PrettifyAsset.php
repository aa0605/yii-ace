<?php
namespace xing\ace\modules\admin\assets\plugins\view;

use xing\ace\modules\admin\assets\AceBundleAsset;

class PrettifyAsset extends AceBundleAsset
{
    public $css = [
        'css/prettify.min.css'
    ];

    public $js = [
        'js/prettify.min.js',
    ];

    public $depends = [
        'yii\bootstrap\BootstrapPluginAsset',
        'xing\ace\modules\admin\assets\FontAwesomeAsset',
    ];
}