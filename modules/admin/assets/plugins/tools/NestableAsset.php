<?php
namespace xing\ace\modules\admin\assets\plugins\tools;

use xing\ace\modules\admin\assets\AceBundleAsset;

class NestableAsset extends AceBundleAsset
{
    public $js = [
        'js/jquery.nestable.min.js'
    ];

    public $depends = [
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}