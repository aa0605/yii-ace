<?php
namespace xing\ace\modules\admin\assets\plugins\tools;

use xing\ace\modules\admin\assets\AceBundleAsset;

/**
 * Class GritterAsset
 * @package xing\ace\modules\admin\assets\plugins\tools
 */
class GritterAsset extends AceBundleAsset
{
    public $js = [
        'js/jquery.gritter.min.js'
    ];

    public $depends = [
        'xing\ace\modules\admin\assets\plugins\JqueryUIAsset'
    ];
}