<?php
namespace xing\ace\modules\admin\assets\plugins\tools;

use xing\ace\modules\admin\assets\AceBundleAsset;

class ValidateAsset extends AceBundleAsset
{
    public $js = [
        'js/jquery.validate.min.js'
    ];

    public $depends = [
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}