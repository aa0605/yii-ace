<?php
namespace xing\ace\modules\admin\assets\plugins\tools;

use xing\ace\modules\admin\assets\AceBundleAsset;

class BootboxAsset extends AceBundleAsset
{
    public $js = [
        'js/bootbox.js'
    ];

    public $depends = [
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}