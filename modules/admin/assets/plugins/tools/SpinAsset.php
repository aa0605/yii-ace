<?php
namespace xing\ace\modules\admin\assets\plugins\tools;

use xing\ace\modules\admin\assets\AceBundleAsset;

/**
 * Class SpinAsset
 * @package xing\ace\modules\admin\assets\plugins\tools
 */
class SpinAsset extends AceBundleAsset
{
    public $js = [
        'js/spin.js'
    ];

    public $depends = [
        'xing\ace\modules\admin\assets\plugins\JqueryUIAsset'
    ];
}