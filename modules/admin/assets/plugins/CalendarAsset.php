<?php
namespace xing\ace\modules\admin\assets\plugins;

use xing\ace\modules\admin\assets\AceBundleAsset;

class CalendarAsset extends AceBundleAsset
{
    public $css = [
        'css/fullcalendar.min.css'
    ];

    public $js = [
        'js/moment.min.js',
        'js/fullcalendar.min.js',
    ];

    public $depends = [
        'xing\ace\modules\admin\assets\plugins\JqueryUIAsset',
        'xing\ace\modules\admin\assets\plugins\tools\BootboxAsset',
    ];
}