<?php
namespace xing\ace\modules\admin\assets\plugins\form;

use xing\ace\modules\admin\assets\AceBundleAsset;

class MarkdownAsset extends AceBundleAsset
{
    public $js = [
        'js/markdown.min.js',
        'js/bootstrap-markdown.min.js'
    ];

    public $depends = [
        'xing\ace\modules\admin\assets\plugins\JqueryUIAsset'
    ];
}