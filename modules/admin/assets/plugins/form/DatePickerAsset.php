<?php
namespace xing\ace\modules\admin\assets\plugins\form;

use xing\ace\modules\admin\assets\AceBundleAsset;

class DatePickerAsset extends AceBundleAsset
{
    public $css = [
        'css/bootstrap-datepicker3.min.css'
    ];

    public $js = [
        'js/bootstrap-datepicker.min.js'
    ];

    public $depends = [
        'xing\ace\modules\admin\assets\plugins\JqueryUIAsset'
    ];
}