<?php
namespace xing\ace\modules\admin\assets\plugins\form;

use xing\ace\modules\admin\assets\AceBundleAsset;

class DropZoneAsset extends AceBundleAsset
{
    public $css = [
        'css/dropzone.min.css'
    ];

    public $js = [
        'js/dropzone.min.js'
    ];

    public $depends = [
        'yii\bootstrap\BootstrapPluginAsset',
        'xing\ace\modules\admin\assets\FontAwesomeAsset',
    ];
}