<?php
namespace xing\ace\modules\admin\assets\plugins\form;

use xing\ace\modules\admin\assets\AceBundleAsset;

class ColorPickerAsset extends AceBundleAsset
{
    public $css = [
        'css/bootstrap-colorpicker.min.css'
    ];

    public $js = [
        'js/bootstrap-colorpicker.min.js',
    ];

    public $depends = [
        'xing\ace\modules\admin\assets\plugins\JqueryUIAsset'
    ];
}