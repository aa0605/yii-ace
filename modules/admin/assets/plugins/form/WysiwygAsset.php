<?php
namespace xing\ace\modules\admin\assets\plugins\form;

use xing\ace\modules\admin\assets\AceBundleAsset;

class WysiwygAsset extends AceBundleAsset
{
    public $js = [
        'js/bootstrap-wysiwyg.min.js'
    ];

    public $depends = [
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}