<?php
namespace xing\ace\modules\admin\assets\plugins\form;

use xing\ace\modules\admin\assets\AceBundleAsset;

class DateTimePickerAsset extends AceBundleAsset
{
    public $css = [
        'css/bootstrap-datetimepicker.min.css'
    ];

    public $js = [
        'js/bootstrap-datetimepicker.min.js',
    ];

    public $depends = [
        'xing\ace\modules\admin\assets\plugins\JqueryUIAsset'
    ];
}