<?php
namespace xing\ace\modules\admin\assets\plugins\form;

use xing\ace\modules\admin\assets\AceBundleAsset;

class ChosenAsset extends AceBundleAsset
{
    public $css = [
        'css/chosen.min.css'
    ];

    public $js = [
        'js/chosen.jquery.min.js'
    ];

    public $depends = [
        'xing\ace\modules\admin\assets\plugins\JqueryUIAsset'
    ];
}