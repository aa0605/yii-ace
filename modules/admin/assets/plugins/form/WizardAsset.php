<?php
namespace xing\ace\modules\admin\assets\plugins\form;

use xing\ace\modules\admin\assets\AceBundleAsset;

class WizardAsset extends AceBundleAsset
{
    public $css = [
        'css/select2.min.css'
    ];

    public $js = [
        'js/wizard.min.js',
        'js/select2.min.js'
    ];

    public $depends = [
        'xing\ace\modules\admin\assets\plugins\JqueryUIAsset',
        'xing\ace\modules\admin\assets\plugins\tools\ValidateAsset',
        'xing\ace\modules\admin\assets\plugins\tools\AdditionalMethodsAsset',
        'xing\ace\modules\admin\assets\plugins\tools\BootboxAsset',
        'xing\ace\modules\admin\assets\plugins\form\MaskedInputAsset',
    ];
}