<?php

namespace xing\ace\modules\admin\assets;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * Configuration for Ace Admin client script files
 */
class AppAsset extends AssetBundle
{
    public $sourcePath = '@xing/ace/assets/web';

    public $css = [
        'css/style.css'
    ];

    public $js = [
        'js/tools.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'xing\ace\modules\admin\assets\AceAsset',
        //'xing\ace\modules\admin\assets\plugins\JqueryUIAsset',
        'xing\ace\modules\admin\assets\AceScriptAsset',
    ];
}