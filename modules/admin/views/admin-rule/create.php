<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model xing\ace\modules\admin\models\AdminRule */

$this->title = '增加Admin Rule';
$this->params['breadcrumbs'][] = ['label' => 'Admin Rules', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admin-rule-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
