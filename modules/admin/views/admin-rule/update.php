<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model xing\ace\modules\admin\models\AdminRule */

$this->title = '修改';
$this->params['breadcrumbs'][] = ['label' => 'Admin Rules', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = '修改';
?>
<div class="admin-rule-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
