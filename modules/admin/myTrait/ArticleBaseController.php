<?php
/**
 * Created by PhpStorm.
 * User: xing.chen
 * Date: 2018/9/16
 * Time: 23:30
 */

namespace xing\ace\modules\admin\myTrait;

use common\exception\ModelException;
use common\logic\article\TemplateLogic;
use common\modules\article\Article;
use Yii;
use common\modules\article\ArticleCategory;

trait ArticleBaseController
{

    public function actionIndex()
    {

        $this->layout = false;
        $template = TemplateLogic::getTemplatePath('index');
        return $this->render($template);
    }

    public function actionLists()
    {
        try {

            $catDir = \common\logic\article\ArticleCategoryLogic::getCurrentCategoryDir();
            $category = ArticleCategory::dirByCategory($catDir);
            if (empty($category)) throw new \Exception('没有这个栏目');

            $template = TemplateLogic::getTemplatePath($category->categoryTemplate ?: 'lists');

            return $this->render($template, ['catDir' => $catDir]);
        } catch (\Exception $e) {
            $this->showError($e);
        }
    }

    public function actionView()
    {
        try {

            $articleId = Yii::$app->request->get('articleId');
            $article = Article::one($articleId);
            if (empty($article)) throw new \Exception('没有这篇文章');

            $catDir = \common\logic\article\ArticleCategoryLogic::getCurrentCategoryDir();
            $category = ArticleCategory::dirByCategory($catDir);
            if (empty($category)) throw new \Exception('没有这个栏目');

            $templateName = $article->template ?: $category->articleTemplate ?: 'view';
            $template = TemplateLogic::getTemplatePath($templateName);

            return $this->render($template, ['catDir' => $catDir, 'article' => $article]);
        } catch (\Exception $e) {
            $this->showError($e);
        }
    }
}