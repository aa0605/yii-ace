<?php
// +----------------------------------------------------------------------
// | When work is a pleasure, life is a joy!
// +----------------------------------------------------------------------
// | User: ShouKun Liu  |  Email:24147287@qq.com  | Time:2016/12/10 23:56
// +----------------------------------------------------------------------
// | TITLE:基础类
// +----------------------------------------------------------------------

namespace xing\ace\modules\admin\controllers;

use xing\ace\modules\admin\myTrait\AdminLog;
use xing\ace\modules\admin\myTrait\Rbac;
use xing\ace\modules\admin\map\TimezoneMap;
use Yii;
use yii\helpers\Url;
use xing\ace\modules\admin\helps\Tree;
use yii\web\Controller;
use xing\ace\modules\admin\models\AdminRole;


/**
 * Class BaseController
 * @package backend\controllers
 */
class BaseController extends Controller
{
    public $menu;
    public $menuHtml; //左侧栏目菜单
    public $uid; //当前登录用户id
    public $username; //当前登录用户名
    public $password_hash; //当前登录用户名密码
    public $userrole; //角色名称
    public $userphone;//当前登录手机号
    public $nowTime ; //当前时间戳

    use Rbac;
    use AdminLog;



    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /*
     * 页面成功跳转
     * $param string $msg 提示信息
     * $param string $url 跳转地址
     */
    public function alert_succ($msg,$url){
        return $this->render('@app/views/public/alert',['msg'=>$msg,'url'=>$url,'type'=>'success']);
    }
    /*
     * 页面失败跳转
     * $param string $msg 提示信息
     * $param string $url 跳转地址
     */
    public function alert_err($msg,$url){
        return $this->render('@app/views/public/alert',['msg'=>$msg,'url'=>$url,'type'=>'error']);
    }

    public function beforeAction($action)
    {
        parent::beforeAction($action);

        if ($this->isLogin($action)) {
            $this->uid = Yii::$app->user->identity->getId(); //获取后台管理员ID

            $userArr = Yii::$app->db->createCommand("select mobile,username,password_hash,role_id,timezone from admin_user where id = ".$this->uid)->queryOne();

            if (array_key_exists('timezone',$_GET)){
                date_default_timezone_set(TimezoneMap::$TIMEZONES[$_GET['timezone'] ?? 8]);  //切换时区
            }else{
                date_default_timezone_set(TimezoneMap::$TIMEZONES[$userArr['timezone']]);  //时区设置
            }

            $this->username = $userArr['username'];
            $this->password_hash = $userArr['password_hash'];
            $this->nowTime = time();
            $this->userphone = $userArr['mobile'];
            $ruleAll = AdminRole::find()->select('name')->andWhere(['in','id',explode(',',$userArr['role_id'])])->createCommand()->queryAll();
            $this->userrole = implode(array_column($ruleAll,'name'));

            if ($this->route == 'index/index'){ //如果首页才查询菜单
                $this->menu = AdminRole::getRule(Yii::$app->user->identity->role_id,'2');
                $this->menuHtml = self::buildMenuHtml(Tree::makeTree($this->menu));
            }
            if (!$this->verifyRule($this->route)) {
                //todo 没有权限处理
                die('你没有权限');
            }
        }
        return true;
    }

    /**
     * 验证登入
     * @return bool
     */
    protected function isLogin()
    {
        if (Yii::$app->user->isGuest) {
            $allowUrl = ['site/logout', 'site/login'];
            if (in_array($this->route, $allowUrl) == false) {
                $loginUrl = Url::toRoute('/site/login');
                header("Location: $loginUrl");
                exit();
            } else {
                return false;
            }
        } else {
            return true;
        }

    }

    /**
     * 生成
     * @param $data
     * @param string $html
     * @return string
     */
    private static function buildMenuHtml($data, $html = '')
    {
        if (empty($data)){
            return "";
        }
        foreach ($data as $k => $v) {
            if (isset($v['type']) && $v['type'] != 2 && $v['status'] == 1) {

                $html .= '<li >';
                //需要验证是否有子菜单
                if (isset($v['children']) && is_array($v['children'])) {
                    $html .= '<a href="javascript::(0)" class="dropdown-toggle">';
                } else {
                    $html .= '<a href="javascript:openapp(\' ' . Url::toRoute($v['route']) . '\',\'' . $v['id'] . '\',\'' . $v['title'] . '\',true);" class="">';
                }
                //图标
                $html .= '<i class="menu-icon ' . $v['icon'] . '"></i>';
                //名称
                $html .= '   <span class="menu-text">' . $v['title'] . '</span>';

                if (isset($v['children']) && is_array($v['children'])) {
                    $html .= '<b class="arrow fa fa-angle-down"></b></a>';
                } else {
                    $html .= '<b class="arrow fa s"></b></a>';
                }

                //需要验证是否有子菜单
                if (isset($v['children']) && is_array($v['children'])) {
                    $html .= ' <b class="arrow"></b>';
                    $html .= '<ul class="submenu nav-hide" style="display: none;">';
                    $html .= self::buildMenuHtml($v['children']);
                    //验证是否有子订单
                    $html .= '</ul>';
                }
                $html .= '</li>';
            }
        }
        return $html;

    }

}

