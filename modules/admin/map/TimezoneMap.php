<?php

namespace xing\ace\modules\admin\map;

class TimezoneMap
{
    # 默认时区
    const TIMEZONE_DEFAULT = 8;

    public static $TIMEZONE_LABELS = [
        -8 => '西八区，美国洛杉矶',
        -5 => '西六区，墨西哥城',
        -5 => '西五区，美国纽约',
        0 => '中时区，英国伦敦',
        2 => '东二区，俄罗斯莫斯科',
        4 => '东三区，伊朗德黑兰',
//        5 => '东五区，印度新德里',
        6 => '东六区，乌鲁木齐',
        8 => '东八区，北京/上海',
        9 => '东九区，日本东京',
    ];

    public static $TIMEZONES = [
        -8 => 'America/Los_Angeles',
        -6 => 'America/Mexico_City',
        -5 => 'America/New_York',
        0 => 'Europe/London',
        2 => 'Europe/Moscow',
        4 => 'Asia/Tehran',
//        5 => 'Asia/Tehran',
        6 => 'Asia/Urumqi',
        8 => 'Asia/Shanghai',
        9 => 'Asia/Tokyo',
    ];
}
